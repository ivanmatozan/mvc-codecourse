<?php

// Composer autoload
require __DIR__ . '/../vendor/autoload.php';

$app = new App\Application();

$container = $app->getContainer();

$container['errorHandler'] = function () {
    return function ($response) {
        return $response->setBody('Page not found')->withStatus(404);
    };
};

$container['config'] = function () {
    return [
        'db_driver' => 'mysql',
        'db_host' => 'localhost',
        'db_name' => 'codecourse_mvc',
        'db_user' => 'root',
        'db_password' => '',
    ];
};

$container['db'] = function ($c) {
    $config = $c->config;

    return new PDO(
        $config['db_driver'] . ':host=' . $config['db_host'] . ';dbname=' . $config['db_name'],
        $config['db_user'],
        $config['db_password']
    );
};

$app->get('/', [\App\Controllers\HomeController::class, 'index']);

$app->get('/test', [App\Controllers\HomeController::class, 'test']);

$app->get('/users', [new App\Controllers\HomeController($container->db), 'users']);

$app->get('/testis',  function ($response) {
    var_dump($response);
    die();
});

$app->post('/login', function () {
    echo 'Login';
});

$app->map('/items', function () {
    echo 'Items';
}, ['GET', 'POST']);

$app->run();