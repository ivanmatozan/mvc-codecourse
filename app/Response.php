<?php

namespace App;

class Response
{
    protected $body;
    protected $statusCode = 200;

    protected $headers = [];

    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function withStatus(int $statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function withJson($body)
    {
        $this->withHeader('Content-type', 'application/json');
        $this->body = json_encode($body);

        return $this;
    }

    public function withHeader($name, $value)
    {
        $this->headers[] = [$name, $value];

        return $this;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }
}