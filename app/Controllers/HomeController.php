<?php

namespace App\Controllers;

use App\Models\User;
use App\Response;
use PDO;

class HomeController
{
    protected $db;

    public function __construct(PDO $db)
    {
        $this->db = $db;
    }

    public function index(Response $response)
    {
        return $response->setBody('Home');
    }

    public function test()
    {
        echo 'Home';
    }

    public function users(Response $response)
    {
        $users = $this->db->query('SELECT * FROM user')
            ->fetchAll(PDO::FETCH_CLASS, User::class);

        return $response->withJson($users);
    }
}